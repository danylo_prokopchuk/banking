import { TokenManager } from './../services/auth/tokenProvider';
export class HttpErrorHelper {

    public static getError(response: any): string {

        if(response.status === 401){
            TokenManager.deleteToken();            
        }

        if (response.status !== 400)
            return "Unknown error";


        if (response.error['errorMsg']) {
            return response.error.errorMsg[0]
        }
        else
            return response.error[0][0];
    }
}
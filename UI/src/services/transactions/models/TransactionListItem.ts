export class TransactionListItem{

    public amount: number;

    public operationType: number;

    public createdAt : Date;

    public reciverName : string;

    public senderName : string;

    public isOwner : boolean;

}
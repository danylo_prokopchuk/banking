import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { TokenManager } from '../auth/tokenProvider';
import { PaginationResult } from '../commonModels/PaginationResult';
import { TransactionListItem } from './models/TransactionListItem';

@Injectable()
export class TransactionsService {


    constructor(private httpClient: HttpClient) {

    }

    public getUserTransactions(take: number, skip: number) : Observable<PaginationResult<TransactionListItem>> {
        return this.httpClient.get<PaginationResult<TransactionListItem>>(environment.apiUri + "/transactions?take=" + take + "&skip=" + skip, 
        { headers: TokenManager.getHeaderWithToken() });
    }

}
import { HttpHeaders } from '@angular/common/http';
import { EventEmitter } from "@angular/core";

export class TokenManager {

    private static readonly storageKey = "apiToken";

    public static readonly tokenUpdated: EventEmitter<boolean> = new EventEmitter();

    public static getToken(): string {
        return localStorage.getItem(TokenManager.storageKey)
    }

    public static setToken(token: string) {
        localStorage.setItem(TokenManager.storageKey, token);
        this.tokenUpdated.emit(true);
    }

    public static deleteToken() {
        localStorage.removeItem(TokenManager.storageKey);
        this.tokenUpdated.emit(false);
    }

    public static isTokenExist(): boolean {
        var token = localStorage.getItem(TokenManager.storageKey);
        return (token !== null && token !== "");
    }

    public static getHeaderWithToken(): HttpHeaders {
        return new HttpHeaders({ "authorization": "Bearer " + this.getToken() });
    }
}
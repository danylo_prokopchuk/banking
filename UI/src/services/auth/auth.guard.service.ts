import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { TokenManager } from "./tokenProvider";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        var canActivate = TokenManager.isTokenExist();
        if (!canActivate) {
            this.router.navigate(["/login"])
        }

        return canActivate;
    }
}
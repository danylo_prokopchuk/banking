import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class AuthService {

    constructor(private http: HttpClient) { }

    register(userName: string, password: string) : Observable<void> {
        return this.http.post<void>(environment.apiUri + "/account/register",
            { userName: userName, password: password });
    }

    login(userName: string, password: string) : Observable<string> {
        return this.http.post<string>(environment.apiUri + "/account/login",
            { userName: userName, password: password });
    }
 
}
export class PaginationInput{

    public take: number;
    
    public skip: number;
}
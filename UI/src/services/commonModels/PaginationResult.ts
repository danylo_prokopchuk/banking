export class PaginationResult<T>{

    public pageNumber: number;

    public pageSize: number;

    public totalRecords: number;

    public totalPages: number;

    public items: T[];
}
import { Observable } from 'rxjs';
import { TokenManager } from './../auth/tokenProvider';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";

@Injectable()
export class OperationsService {

    constructor(private http: HttpClient) {


    }

    public depositMoney(amount: number) : Observable<void> {
        return this.http.post<void>(environment.apiUri + "/operations/deposit", { amount: amount },
            {
                headers: TokenManager.getHeaderWithToken()
            });
    }

    public withdrawMoney(amount: number) : Observable<void> {
        return this.http.post<void>(environment.apiUri + "/operations/withdraw", { amount: amount },
            {
                headers: TokenManager.getHeaderWithToken()
            });
    }

    public transferMoney(reciver: string, amount: number) : Observable<void> {
        return this.http.post<void>(environment.apiUri + "/operations/transfer", { amount: amount, recieverId: reciver },
            {
                headers: TokenManager.getHeaderWithToken()
            });
    }
}
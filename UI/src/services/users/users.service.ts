import { IdName } from './../commonModels/IdName';
import { Observable } from 'rxjs';
import { TokenManager } from './../auth/tokenProvider';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";

@Injectable()
export class UsersService {


    constructor(private http: HttpClient) {

    }

    public getUsersList() : Observable<IdName[]> {
        return this.http.get<IdName[]>(environment.apiUri + "/users", {
            headers: TokenManager.getHeaderWithToken()
        });
    }
}
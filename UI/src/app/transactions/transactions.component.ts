import { EventEmitter, Output } from '@angular/core';
import { HttpErrorHelper } from './../../helpers/HttpErrorHelper';
import { TransactionListItem } from './../../services/transactions/models/TransactionListItem';
import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../../services/transactions/transactions.service';
import { PaginationResult } from '../../services/commonModels/PaginationResult';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  transactions: PaginationResult<TransactionListItem>
  public readonly PAGE_SIZE = 10;


  constructor(private transactionsService: TransactionsService) { }

  ngOnInit() {
    this.getTableData(this.PAGE_SIZE, 0);
  }

  onPagination(e: PageEvent) {   
    var take = e.pageSize;
    var skip = e.pageIndex * take;
    this.getTableData(take, skip);
  }

  private getTableData(take: number, skip: number) {
    this.transactionsService.getUserTransactions(take, skip)
      .subscribe(
        (data) => {
          this.transactions = data;
        },
        (errorResponse) => HttpErrorHelper.getError(errorResponse));
  }
}

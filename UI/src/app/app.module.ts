import { OperationsService } from './../services/operations/operations.service';
import { UsersService } from './../services/users/users.service';
import { AuthGuard } from '../services/auth/auth.guard.service';
import { AuthService } from '../services/auth/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { DepositComponent } from './deposit/deposit.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { TransferComponent } from './transfer/transfer.component';
import { Routes, RouterModule } from '@angular/router';

//import { setTheme } from 'ngx-bootstrap/utils';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatMenuModule, MatIconModule, MatToolbarModule, MatFormFieldModule, MatCardModule, MatInputModule, ShowOnDirtyErrorStateMatcher, ErrorStateMatcher, MatOptionModule, MatSelectModule, MatPaginatorModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TransactionsService } from '../services/transactions/transactions.service';

const appRoutes: Routes = [
  { path: '', component: TransactionsComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'deposit', component: DepositComponent, canActivate: [AuthGuard] },
  { path: 'withdraw', component: WithdrawComponent, canActivate: [AuthGuard] },
  { path: 'transfer', component: TransferComponent, canActivate: [AuthGuard] },
]



@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    TransactionsComponent,
    DepositComponent,
    WithdrawComponent,
    TransferComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatMenuModule,
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    MatPaginatorModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatToolbarModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [],
  providers: [
    AuthService, 
    UsersService, 
    OperationsService, 
    TransactionsService, 
    AuthGuard], 
  bootstrap: [AppComponent]
})

export class AppModule {

}

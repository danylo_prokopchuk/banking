import { AuthService } from '../services/auth/auth.service';
import { TokenManager } from '../services/auth/tokenProvider';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  isAuthorized = false;

  constructor(private router: Router) {
    this.isAuthorized = TokenManager.isTokenExist();
    TokenManager.tokenUpdated.subscribe((res) => {
      var prevValue = this.isAuthorized;
      this.isAuthorized = res;

      if (prevValue && !res)
        router.navigate(['/login']);
    });
  }

  onLogout() {
    TokenManager.deleteToken();
    this.router.navigate(["/login"]);
  }


}

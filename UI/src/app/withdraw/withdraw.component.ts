import { OperationsService } from './../../services/operations/operations.service';
import { HttpErrorHelper } from './../../helpers/HttpErrorHelper';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {

  withdrawSuccess = false;
  withdrawError = "";

  withdrawForm: FormGroup = new FormGroup({
    "amount": new FormControl("", [Validators.required, Validators.min(1)])
  });

  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;

  constructor(private operationService: OperationsService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.withdrawForm.valid) {
      this.operationService.withdrawMoney(this.withdrawForm.controls.amount.value)
        .subscribe(
          () => {
            this.withdrawSuccess = true;
            this.formDirective.resetForm();
            setTimeout(() => {
              this.withdrawSuccess = false;
            }, 2000);
          },
          (errorResponse) => {
            this.withdrawError = HttpErrorHelper.getError(errorResponse);
            
          }
        );
    }
  }

}

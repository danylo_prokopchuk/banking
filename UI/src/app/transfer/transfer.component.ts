import { OperationsService } from './../../services/operations/operations.service';
import { UsersService } from './../../services/users/users.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpErrorHelper } from '../../helpers/HttpErrorHelper';
import { IdName } from '../../services/commonModels/IdName';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  transferSuccess = false;
  transferError = "";
  users: IdName[];
  transferForm = new FormGroup({
    'users': new FormControl("", Validators.required),
    "amount": new FormControl("", [Validators.required, Validators.min(1)])
  });

  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;


  constructor(private usersService: UsersService, private operationsService: OperationsService) { }

  ngOnInit() {
    this.usersService.getUsersList().subscribe(
      (data) => {
        this.users = data;
      },
      (errorResponse) => {
        this.transferError = HttpErrorHelper.getError(errorResponse);
      });
  }

  onSubmit() {
    if (this.transferForm.valid) {
      this.operationsService.transferMoney(this.transferForm.controls.users.value, this.transferForm.controls.amount.value)
        .subscribe(
          () => {
            this.transferSuccess = true;
            this.formDirective.resetForm();
            setTimeout(() => {
              this.transferSuccess = false;
            }, 2000);
          },
          (errorResponse) => {
            this.transferError = HttpErrorHelper.getError(errorResponse);
          });
    }
  }
}

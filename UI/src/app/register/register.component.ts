import { HttpErrorHelper } from './../../helpers/HttpErrorHelper';
import { PasswordValidator } from './../../helpers/PasswordValidator';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registrationForm: FormGroup;
  passwordsEquals = false;
  registrationError = "";

  constructor(
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {

    this.registrationForm = new FormGroup({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required),
      'confirmPassword': new FormControl('', Validators.required)
    }, PasswordValidator.MatchPassword);
  }


  onFormSubmited() {
    if (this.registrationForm.valid) {
      this.authService.register(this.registrationForm.controls.username.value, this.registrationForm.controls.password.value)
        .subscribe(
          (done) => {            
            this.router.navigate(["/login"]);
          },
          (errorResponse) => {
            this.registrationError = HttpErrorHelper.getError(errorResponse);
          })
    }
  }
}

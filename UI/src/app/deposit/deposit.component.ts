import { HttpErrorHelper } from './../../helpers/HttpErrorHelper';
import { OperationsService } from './../../services/operations/operations.service';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {

  depositeSuccess = false;
  depositeError = "";

  depositForm: FormGroup = new FormGroup({
    "amount": new FormControl("", [Validators.required, Validators.min(1)])
  });

  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;

  constructor(private operationService: OperationsService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.depositForm.valid) {
      this.operationService.depositMoney(this.depositForm.controls.amount.value)
        .subscribe(
          () => {             
            this.depositeSuccess = true;
            this.formDirective.resetForm();
            setTimeout(() => {
              this.depositeSuccess = false;
            }, 2000);
          },
          (errorResponse) => {
            this.depositeError = HttpErrorHelper.getError(errorResponse);
          });
    }
  }
}

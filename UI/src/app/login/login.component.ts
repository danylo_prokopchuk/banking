import { HttpErrorHelper } from './../../helpers/HttpErrorHelper';
import { AuthService } from '../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TokenManager } from '../../services/auth/tokenProvider';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginError = "";
  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });
  }


  onSubmit() {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value)
        .subscribe(
          (successResponse) =>{ 
            TokenManager.setToken(successResponse);
            this.router.navigate(["/"]) ;
          },
          (errorResponse) => this.loginError = HttpErrorHelper.getError(errorResponse)
        );
    }
  }
}

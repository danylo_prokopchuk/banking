﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Banking.Common.Models
{

    public class PaginationResult<T>
    {
        public const int DEFAULT_PAGE_SIZE = 10;
        public const int DEFAULT_PAGE_NUMBER = 1;
        private int _lastSkipedValue = -1;


        public PaginationResult(int pageNumber, int pageSize)
        {
            PageSize = pageSize;
            PageNumber = pageNumber;
        }

        public PaginationResult(int pageNumber) : this(pageNumber, DEFAULT_PAGE_SIZE) { }

        public PaginationResult() : this(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE) { }

        public IEnumerable<T> Items = new T[0];

        private int _pageNumber;
        public int PageNumber
        {
            get { return _pageNumber; }
            set
            {
                if (value < 1)
                    throw new ArgumentException("Page number must be greater than 1");

                _pageNumber = value;
            }
        }

        private int _totalRecords;
        public int TotalRecords
        {
            get { return _totalRecords; }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Page size must be greater than 0");

                _totalRecords = value;
            }
        }

        private int _pageSize;
        public int PageSize
        {
            get
            {

                //All records case
                if (_pageSize == 0)
                    return TotalRecords;

                return _pageSize;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentException("Page size must be greater than 0");

                _pageSize = value;

                //Update Page size using new information
                if (_lastSkipedValue != -1)
                    PageNumberFromSkiped(_lastSkipedValue);

            }
        }

        public int TotalPages
        {
            get
            {
                var result = 0;

                try
                {
                    result = TotalRecords / PageSize;

                    if ((result * PageSize) < TotalRecords)
                        result++;
                }
                catch (DivideByZeroException) { }

                return result;
            }
        }

        public bool HasPrevious => PageNumber != 1;

        public bool HasNext
        {
            get
            {
                if (Items.Count() < PageSize)
                {
                    return false;
                }

                var result = TotalRecords - (PageNumber * PageSize) > 0;
                return result;
            }
        }

        public void PageNumberFromSkiped(int recordsAmount)
        {
            var pageNum = recordsAmount / PageSize;
            PageNumber = (pageNum > 0) ? pageNum +1 : 1;
            _lastSkipedValue = recordsAmount;
        }


    }
}
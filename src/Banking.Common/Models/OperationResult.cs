﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.Common.Models
{
    public class OperationResult
    {
        public bool Success { get; set; } = true;

        public string ErrorMessage { get; set; }

        public int ErrorCode { get; set; }
    }

    public class OperationResult<T> : OperationResult
    {
        public T Object { get; set; }
    }
}
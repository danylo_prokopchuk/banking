﻿using System;
using System.Text;

namespace Banking.Common.Authorization
{
    public class PasswordHasher : IPasswordHasher
    {
        private const string Salt = "xiwADjxeVVAkXK7EsoAm";

        public string HashPassword(string password)
        {

            var bytes = new UTF8Encoding().GetBytes($"{Salt}{password}");
            byte[] hashBytes;
            using (var algorithm = new System.Security.Cryptography.SHA512Managed())
            {
                hashBytes = algorithm.ComputeHash(bytes);
            }
            return  Convert.ToBase64String(hashBytes);
            
        }
    }
}

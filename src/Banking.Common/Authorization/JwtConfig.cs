﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.Common.Authorization
{
    public class JwtConfig
    {
        public int LifeTime { get; set; }

        public string Issuer { get; set; }

        public string Key { get; set; }
    }
}

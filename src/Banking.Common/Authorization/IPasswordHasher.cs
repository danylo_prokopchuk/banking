﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.Common.Authorization
{
    public interface IPasswordHasher
    {
        string HashPassword(string password);
    }
}

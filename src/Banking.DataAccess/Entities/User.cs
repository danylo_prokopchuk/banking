﻿using Banking.DataAccess.Entities.Abstract;

namespace Banking.DataAccess.Entities
{
    public class User : EntityWithId
    {
        public string Username { get; set; }

        public string PasswordHash { get; set; }
        
        public decimal Balance { get; set; }
    }
}
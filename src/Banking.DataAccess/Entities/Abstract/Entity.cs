﻿using System;

namespace Banking.DataAccess.Entities.Abstract
{
    public abstract class Entity
    {
    }

    public abstract class EntityWithId : Entity
    {
        public Guid Id { get; set; }
    }
}

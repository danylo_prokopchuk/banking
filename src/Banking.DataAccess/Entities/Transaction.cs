﻿using Banking.DataAccess.Entities.Abstract;
using Banking.DataAccess.Entities.Enums;
using System;

namespace Banking.DataAccess.Entities
{
    public class Transaction : EntityWithId
    {
        public Guid OwnerId { get; set; }

        public Guid? ReceiverId { get; set; }

        public decimal Amount { get; set; }

        public TransactionOperationEnum OperationType { get; set; }

        public DateTime CreatedAt { get; set; }

        public User Reciever { get; set; }

        public User Owner { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.DataAccess.Entities.Enums
{
    public enum TransactionOperationEnum : short
    {
        Deposit,
        Withdraw,
        Transfer
    }
}

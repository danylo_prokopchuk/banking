﻿using Banking.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Banking.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        
        public ApplicationContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(ConfigureUsers);
            modelBuilder.Entity<Transaction>(ConfigureTransactions);

            base.OnModelCreating(modelBuilder);
        }


        private void ConfigureTransactions(EntityTypeBuilder<Transaction> typeBuilder)
        {
            typeBuilder.HasOne(x => x.Owner)
                .WithMany()
                .HasForeignKey(x => x.OwnerId)
                .OnDelete(DeleteBehavior.Restrict);

            typeBuilder.HasOne(x => x.Reciever)
                .WithMany()
                .HasForeignKey(x => x.ReceiverId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);
        }

        private void ConfigureUsers(EntityTypeBuilder<User> entityType)
        {
            entityType.Property(x => x.Username)
                .IsRequired(true);

            entityType.HasIndex(x => x.Username)
                .IsUnique();

            entityType.Property(x => x.Balance)
               .IsConcurrencyToken();
        }
    }
}

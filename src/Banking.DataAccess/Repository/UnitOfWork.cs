﻿using Banking.DataAccess.Interfaces.Repository;
using System.Threading.Tasks;

namespace Banking.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext context;


        public UnitOfWork(ApplicationContext applicationContext)
        {
            context = applicationContext;
        }

        private IUsersRepository _usersRepository;
        public IUsersRepository UsersRepository => _usersRepository ?? (_usersRepository = new UsersRepository(context));

        private ITransactionsRepository _transactionsRepository;
        public ITransactionsRepository TransactionsRepository => _transactionsRepository ?? (_transactionsRepository = new TransactionsRepository(context));


        public Task SubmitChangesAsync()
        {
            return context.SaveChangesAsync();
        }

        public void BeginTransaction()
        {
            context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            context.Database.CommitTransaction();
        }

        public void RollBackTransaction()
        {
            context.Database.RollbackTransaction();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void SubmitChanges()
        {
            context.SaveChanges();
        }
    }
}

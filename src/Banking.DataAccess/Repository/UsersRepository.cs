﻿using Banking.DataAccess.Entities;
using Banking.DataAccess.Interfaces.Repository;
using System.Linq;

namespace Banking.DataAccess.Repository
{
    public class UsersRepository : BaseRepository<User>, IUsersRepository
    {
        public UsersRepository(ApplicationContext context) : base(context)
        {
        }
 
    }
}

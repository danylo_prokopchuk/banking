﻿using Banking.DataAccess.Entities;
using Banking.DataAccess.Interfaces.Repository;

namespace Banking.DataAccess.Repository
{
    public class TransactionsRepository : BaseRepository<Transaction>, ITransactionsRepository
    {
        public TransactionsRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

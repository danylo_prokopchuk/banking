﻿using Banking.DataAccess.Entities.Abstract;
using Banking.DataAccess.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.DataAccess.Repository
{
    public abstract class BaseRepository<T> : IRepository<T> where T : Entity
    {
        protected BaseRepository(ApplicationContext context)
        {
            Context = context;
        }

        protected ApplicationContext Context { get; }

        public bool IsDisposed { get; private set; }



        public virtual T Get(object id)
        {
            return Context.Set<T>().Find(id);
        }

        public virtual async Task<T> GetAsync(object id)
        {
            var result = Context.Set<T>().Find(id);
            return await Task.FromResult(result);
        }

        public virtual IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }


        public virtual void Insert(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public virtual void DeleteRange(IQueryable<T> entities)
        {
            Context.Set<T>().RemoveRange(entities);
        }


        public virtual void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void Dispose()
        {
            if (IsDisposed)
                return;

            Context?.Dispose();
            IsDisposed = true;
        }
    }
}

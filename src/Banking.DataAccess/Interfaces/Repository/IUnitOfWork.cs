﻿using System;
using System.Threading.Tasks;

namespace Banking.DataAccess.Interfaces.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IUsersRepository UsersRepository { get; }
        ITransactionsRepository TransactionsRepository { get; }

        void BeginTransaction();
        void CommitTransaction();
        void RollBackTransaction();

        Task SubmitChangesAsync();
        void SubmitChanges();
    }
}

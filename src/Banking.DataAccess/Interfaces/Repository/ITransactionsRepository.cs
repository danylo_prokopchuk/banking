﻿using Banking.DataAccess.Entities;

namespace Banking.DataAccess.Interfaces.Repository
{
    public interface ITransactionsRepository : IRepository<Transaction>
    {
    }
}

﻿using Banking.DataAccess.Entities.Abstract;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.DataAccess.Interfaces.Repository
{

    public interface IRepository<T> : IDisposable where T : Entity
    {
        T Get(object id);
        Task<T> GetAsync(object id);
        IQueryable<T> GetAll();

        void Insert(T entity);
        void Delete(T entity);
        void Update(T entity);
        void DeleteRange(IQueryable<T> entities);

    }
}

﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Banking.API.Extentions
{
    public static class ClaimExtentions
    {
        public static Guid GetId(this ClaimsPrincipal claimsPrincipal)
        {
            var strGuid = claimsPrincipal.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti)?.Value;
            return string.IsNullOrEmpty(strGuid) ? Guid.Empty : Guid.Parse(strGuid);
        }
    }
}

﻿using Banking.API.Extentions;
using Banking.Business.Transactions;
using Banking.Business.Transactions.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Banking.API.Controllers
{
    [Authorize]
    public class TransactionsController : BaseApiController
    {
        private readonly ITransactionsService _transactionsService;

        public TransactionsController(ITransactionsService transactionsService)
        {
            _transactionsService = transactionsService;
        }


        [HttpGet]
        public async Task<IActionResult> Get(int take, int skip)
        {
            var transactions = await _transactionsService.TransactionsListAsync(new TransactionsListInput
            {
                Skip = skip,
                Take = take,
                UserId = User.GetId()
            });

            return new JsonResult(transactions);
        }


        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            return Ok();
        }
    }
}
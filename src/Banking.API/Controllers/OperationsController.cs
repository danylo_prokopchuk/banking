﻿using Banking.API.Extentions;
using Banking.API.Models.Operations;
using Banking.Business.Operations;
using Banking.Business.Operations.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Banking.API.Controllers
{
    [Authorize]
    public class OperationsController : BaseApiController
    {
        private readonly IOperationsService _operationsService;

        public OperationsController(IOperationsService operationsService)
        {
            _operationsService = operationsService;
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> Deposit(DepositModel model)
        {
            var result = await _operationsService.DepositMoneyAsync(new DepositInput
            {
                Amount = model.Amount,
                UserId = User.GetId()
            });

            return FromOperationResult(result);
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> Withdraw(WithdrawModel model)
        {
            var result = await _operationsService.WithdrawMoneyAsync(new WithdrawInput
            {
                Amount = model.Amount,
                UserId = User.GetId()
            });

            return FromOperationResult(result);
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> Transfer(TransferMoneyModel model)
        {
            var result = await _operationsService.TransferMoneyAsync(new TransferInput
            {
                Amount = model.Amount,
                RecieverId = model.RecieverId,
                SenderId = User.GetId()
            });

            return FromOperationResult(result);
        }

    }
}
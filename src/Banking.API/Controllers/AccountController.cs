﻿using Banking.API.Models.Account;
using Banking.Business.Users;
using Banking.Business.Users.Model;
using Banking.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Banking.API.Controllers
{

    public class AccountController : BaseApiController
    {
        private readonly IUsersService _usersService;
        private readonly JwtFactory _jwtFactory;

        public AccountController(
            IUsersService usersService,
            JwtFactory jwtFactory)
        {
            _usersService = usersService;
            _jwtFactory = jwtFactory;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _usersService.FindUserAsync(new FindUserInput
            {
                Password = model.Password,
                Username = model.Username
            });

            if (user == null)
                return ResponseError("Invalid login or password");

            var token = _jwtFactory.GenerateToken(user);
            return new JsonResult(token);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            var result = await _usersService.CreateAsync(new CreateUserInput
            {
                Username = model.Username,
                Password = model.Password
            });

            return FromOperationResult(result);
        }
    }
}
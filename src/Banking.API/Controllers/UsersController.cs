﻿using Banking.API.Extentions;
using Banking.Business.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.API.Controllers
{
    [Authorize]
    public class UsersController : BaseApiController
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }


        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var dictionary = (await _usersService.GetUsersAsync())
                            .Where(x => x.Id != User.GetId());

            return new JsonResult(dictionary);
        }
    }
}
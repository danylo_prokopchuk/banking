﻿using Banking.API.Attributes;
using Banking.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace Banking.API.Controllers
{
    [ValidateModel]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public abstract class BaseApiController : ControllerBase
    {


        protected IActionResult ResponseError(string error)
        {
            return BadRequest(new { errorMsg = new string[] { error } });
        }


        protected IActionResult FromOperationResult(OperationResult operationResult)
        {
            if (operationResult.Success)
                return Ok();
            else
                return BadRequest(new { errorMsg = new string[] { operationResult.ErrorMessage } });
        }

        protected IActionResult FromOperationResult<T>(OperationResult<T> operationResult)
        {
            if (operationResult.Success)
                return new JsonResult(operationResult.Object);
            else
                return BadRequest(new { errorMsg = new string[] { operationResult.ErrorMessage } });
        }
    }
}
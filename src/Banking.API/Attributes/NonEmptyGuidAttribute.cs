﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Banking.API.Attributes
{
    public class NonEmptyGuidAttribute : ValidationAttribute
    {

        private const string DefaultErrorMessage = "'{0}' does not contain a valid guid";

        public NonEmptyGuidAttribute() : base(DefaultErrorMessage)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return (!Guid.TryParse(value?.ToString(), out Guid guid))
                    ? new ValidationResult(FormatErrorMessage(validationContext.DisplayName))
                    : null;
        }
    }
}

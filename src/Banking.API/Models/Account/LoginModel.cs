﻿using System.ComponentModel.DataAnnotations;

namespace Banking.API.Models.Account
{
    public class LoginModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.API.Models.Operations
{
    public class DepositModel
    {
        [Range(1,Int32.MaxValue)]
        public decimal Amount { get; set; }
    }
}

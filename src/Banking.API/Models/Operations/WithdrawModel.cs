﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Banking.API.Models.Operations
{
    public class WithdrawModel
    {
        [Range(1, Int32.MaxValue)]
        public decimal Amount { get; set; }
    }
}

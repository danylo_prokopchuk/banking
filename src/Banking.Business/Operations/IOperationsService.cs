﻿using Banking.Business.Operations.Models;
using Banking.Common.Models;
using System.Threading.Tasks;

namespace Banking.Business.Operations
{
    public interface IOperationsService
    {
        Task<OperationResult> DepositMoneyAsync(DepositInput input);
        Task<OperationResult> WithdrawMoneyAsync(WithdrawInput input);
        Task<OperationResult> TransferMoneyAsync(TransferInput input);
    }
}

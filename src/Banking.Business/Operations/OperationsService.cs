﻿using Banking.Business.Operations.Models;
using Banking.Common.Models;
using Banking.DataAccess.Entities;
using Banking.DataAccess.Entities.Enums;
using Banking.DataAccess.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.Business.Operations
{
    public class OperationsService : IOperationsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private const int CONCURRENCY_ATTEMPTS = 5;

        private enum ErrorsCode
        {
            UserNotFound = 1,
            NotEnoughtFunds = 2,
            InvalidAmount = 3
        }

        public OperationsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<OperationResult> DepositMoneyAsync(DepositInput input)
        {
            var result = new OperationResult();

            if (input.Amount <= 0)
            {
                result.Success = false;
                result.ErrorMessage = "Amount must be greater than zero";
                result.ErrorCode = (int)ErrorsCode.InvalidAmount;
                return result;
            }

            var user = await _unitOfWork.UsersRepository
                  .GetAll()
                  .Where(x => x.Id == input.UserId)
                  .FirstOrDefaultAsync();

            if (user == null)
            {
                result.Success = false;
                result.ErrorMessage = "Requested user was not found";
                result.ErrorCode = (int)ErrorsCode.UserNotFound;
                return result;
            }

            _unitOfWork.TransactionsRepository.Insert(new Transaction
            {
                Amount = input.Amount,
                OperationType = TransactionOperationEnum.Deposit,
                OwnerId = input.UserId,
                CreatedAt = DateTime.UtcNow
            });


            var transactionCompleted = false;
            int attempt = 0;
            while (!transactionCompleted && attempt < CONCURRENCY_ATTEMPTS)
            {
                user.Balance += input.Amount;

                try
                {
                    await _unitOfWork.SubmitChangesAsync();
                    transactionCompleted = true;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    ex.Entries.Single().Reload();
                    attempt++;
                }
            }

            return result;
        }

        public async Task<OperationResult> TransferMoneyAsync(TransferInput input)
        {
            var result = new OperationResult();

            if (input.Amount <= 0)
            {
                result.Success = false;
                result.ErrorMessage = "Amount must be greater than zero";
                result.ErrorCode = (int)ErrorsCode.InvalidAmount;
                return result;
            }

            var reciever = await _unitOfWork.UsersRepository.GetAll()
                .Where(x => x.Id == input.RecieverId)
                .FirstOrDefaultAsync();

            if (reciever == null)
            {
                result.Success = false;
                result.ErrorMessage = "Reciever was not found";
                result.ErrorCode = (int)ErrorsCode.UserNotFound;
                return result;
            }

            var sender = await _unitOfWork.UsersRepository.GetAll()
                .Where(x => x.Id == input.SenderId)
                .FirstOrDefaultAsync();

            if (sender == null)
            {
                result.Success = false;
                result.ErrorMessage = "Sender was not found";
                result.ErrorCode = (int)ErrorsCode.UserNotFound;
                return result;
            }


            if (sender.Balance < input.Amount)
            {
                result.Success = false;
                result.ErrorMessage = "User doesn't have enought funds";
                result.ErrorCode = (int)ErrorsCode.NotEnoughtFunds;

                return result;
            }

            _unitOfWork.TransactionsRepository.Insert(new Transaction
            {
                Amount = input.Amount,
                OwnerId = input.SenderId,
                ReceiverId = input.RecieverId,
                CreatedAt = DateTime.UtcNow,
                OperationType = TransactionOperationEnum.Transfer
            });

            var transactionCompleted = false;
            int attempt = 0;
            while (!transactionCompleted && attempt < CONCURRENCY_ATTEMPTS)
            {
                if (sender.Balance < input.Amount)
                {
                    result.Success = false;
                    result.ErrorMessage = "User doesn't have enought funds";
                    result.ErrorCode = (int)ErrorsCode.NotEnoughtFunds;

                    return result;
                }

                sender.Balance -= input.Amount;
                reciever.Balance += input.Amount;

                try
                {
                    await _unitOfWork.SubmitChangesAsync();
                    transactionCompleted = true;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    foreach (var entry in ex.Entries)
                    {
                        entry.Reload();
                        attempt++;
                    }
                }
            }

            return result;
        }



        public async Task<OperationResult> WithdrawMoneyAsync(WithdrawInput input)
        {
            var result = new OperationResult();

            if (input.Amount <= 0)
            {
                result.Success = false;
                result.ErrorMessage = "Amount must be greater than zero";
                result.ErrorCode = (int)ErrorsCode.InvalidAmount;
                return result;
            }

            var user = await _unitOfWork.UsersRepository
                .GetAll()
                .Where(x => x.Id == input.UserId)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                result.Success = false;
                result.ErrorMessage = "Requested user was not found";
                result.ErrorCode = (int)ErrorsCode.UserNotFound;
                return result;
            }

            if (user.Balance < 0)
            {
                result.Success = false;
                result.ErrorMessage = "User doesn't have enought funds";
                result.ErrorCode = (int)ErrorsCode.NotEnoughtFunds;
                return result;
            }

            _unitOfWork.TransactionsRepository.Insert(new Transaction
            {
                Amount = input.Amount,
                OperationType = TransactionOperationEnum.Withdraw,
                OwnerId = input.UserId,
                CreatedAt = DateTime.UtcNow
            });

            var transactionCompleted = false;
            int attempt = 0;
            while (!transactionCompleted && attempt < CONCURRENCY_ATTEMPTS)
            {

                user.Balance -= input.Amount;

                if (user.Balance < 0)
                {
                    result.Success = false;
                    result.ErrorMessage = "User doesn't have enought funds";
                    result.ErrorCode = (int)ErrorsCode.NotEnoughtFunds;
                    return result;
                }

                try
                {
                    await _unitOfWork.SubmitChangesAsync();
                    transactionCompleted = true;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    ex.Entries.Single().Reload();
                    attempt++;
                }
            }


            return result;
        }
    }
}

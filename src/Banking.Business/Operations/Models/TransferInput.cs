﻿using System;

namespace Banking.Business.Operations.Models
{
    public class TransferInput
    {
        public Guid SenderId { get; set; }

        public Guid RecieverId { get; set; }

        public decimal Amount { get; set; }

    }
}

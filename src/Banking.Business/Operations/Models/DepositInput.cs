﻿using System;

namespace Banking.Business.Operations.Models
{
    public class DepositInput
    {
        public Guid UserId { get; set; }

        public decimal Amount { get; set; }
    }
}

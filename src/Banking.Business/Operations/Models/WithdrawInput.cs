﻿using System;

namespace Banking.Business.Operations.Models
{
    public class WithdrawInput
    {
        public Guid UserId { get; set; }

        public decimal Amount { get; set; }
    }
}

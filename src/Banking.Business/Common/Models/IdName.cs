﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.Business.Common.Models
{
    public class IdName
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}

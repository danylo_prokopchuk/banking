﻿using Banking.Business.Transactions.Models;
using Banking.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Banking.Business.Transactions
{
    public interface ITransactionsService
    {
        Task<PaginationResult<TransactionListItem>> TransactionsListAsync(TransactionsListInput listInput);
    }
}

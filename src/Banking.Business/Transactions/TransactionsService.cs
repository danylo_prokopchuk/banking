﻿using Banking.Business.Transactions.Models;
using Banking.Common.Models;
using Banking.DataAccess.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.Business.Transactions
{
    public class TransactionsService : ITransactionsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TransactionsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<PaginationResult<TransactionListItem>> TransactionsListAsync(TransactionsListInput listInput)
        {
            var result = new PaginationResult<TransactionListItem>();

            if (listInput.Take != 0)
                result.PageSize = listInput.Take;

            if (listInput.Skip != 0)
                result.PageNumberFromSkiped(listInput.Skip);

            var query = _unitOfWork.TransactionsRepository.GetAll()
                .Where(x => x.OwnerId == listInput.UserId || x.ReceiverId == listInput.UserId);

            result.TotalRecords = await query.CountAsync();

            var items = await query
                .OrderByDescending(x => x.CreatedAt)
                .Skip(listInput.Skip)
                .Take(listInput.Take == 0 ? result.TotalRecords : listInput.Take)
                .Select(x => new TransactionListItem
                {
                    Amount = x.Amount,
                    OperationType = x.OperationType,
                    CreatedAt = x.CreatedAt,
                    ReciverName = x.Reciever.Username,
                    SenderName = x.Owner.Username,
                    IsOwner = x.OwnerId == listInput.UserId
                }).ToArrayAsync();

            result.Items = items;

            return result;
        }
    }
}

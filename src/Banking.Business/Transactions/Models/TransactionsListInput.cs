﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.Business.Transactions.Models
{
    public class TransactionsListInput
    {
        public Guid UserId { get; set; }

        public int Take { get; set; } 

        public int Skip { get; set; } 
              
    }
}

﻿using Banking.DataAccess.Entities.Enums;
using System;

namespace Banking.Business.Transactions.Models
{
    public class TransactionListItem
    {
        public decimal Amount { get; set; }

        public TransactionOperationEnum OperationType { get; set; }

        public DateTime CreatedAt { get; set; }

        public string ReciverName { get; set; }

        public string SenderName { get; set; }

        public bool IsOwner { get; set; }
    }
}

﻿using Banking.Business.Common.Models;
using Banking.Business.Users.Model;
using Banking.Common.Authorization;
using Banking.Common.Models;
using Banking.DataAccess.Entities;
using Banking.DataAccess.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.Business.Users
{
    public class UsersService : IUsersService
    {
        private readonly IPasswordHasher _passwordHasher;
        private readonly IUnitOfWork _unitOfWork;

        private enum ErrorsCode
        {
            EmptyUsername = 1,
            EmptyPassword = 2,
            UserExist = 3
        }

        public UsersService(IPasswordHasher passwordHasher, IUnitOfWork unitOfWork)
        {
            _passwordHasher = passwordHasher;
            _unitOfWork = unitOfWork;
        }

        public async Task<OperationResult<Guid>> CreateAsync(CreateUserInput input)
        {
            var result = new OperationResult<Guid>();

            ValidateUserInput(input, result);
            if (!result.Success)
                return result;

            var user = new User
            {
                Id = Guid.NewGuid(),
                Username = input.Username,
                PasswordHash = _passwordHasher.HashPassword(input.Password)
            };

            _unitOfWork.UsersRepository.Insert(user);
            await _unitOfWork.SubmitChangesAsync();

            result.Object = user.Id;
            return result;
        }

        public async Task<User> FindUserAsync(FindUserInput input)
        {
            var user = await _unitOfWork.UsersRepository.GetAll()
                .Where(x => x.Username == input.Username && x.PasswordHash == _passwordHasher.HashPassword(input.Password))
                .FirstOrDefaultAsync();

            return user;
        }

        public async Task<IEnumerable<IdName>> GetUsersAsync()
        {
            var result = await _unitOfWork.UsersRepository.GetAll()
                .Select(x => new IdName { Id = x.Id, Name = x.Username })
                .ToArrayAsync();

            return result;
        }

        private void ValidateUserInput(CreateUserInput userInput, OperationResult operationResult)
        {
            if (string.IsNullOrEmpty(userInput.Username))
            {
                operationResult.Success = false;
                operationResult.ErrorMessage = "Login can't be empty";
                operationResult.ErrorCode = (int)ErrorsCode.EmptyUsername;
            }
            if (string.IsNullOrEmpty(userInput.Password))
            {
                operationResult.Success = false;
                operationResult.ErrorCode = (int)ErrorsCode.EmptyUsername;
                operationResult.ErrorMessage = "Empty passwords not allowed";
            }

            if (_unitOfWork.UsersRepository.GetAll().Any(x => x.Username == userInput.Username))
            {
                operationResult.Success = false;
                operationResult.ErrorCode = (int)ErrorsCode.UserExist;
                operationResult.ErrorMessage = "This user already exist";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.Business.Users.Model
{
    public class FindUserInput
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}

﻿using Banking.Business.Common.Models;
using Banking.Business.Users.Model;
using Banking.Common.Models;
using Banking.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Banking.Business.Users
{
    public interface IUsersService
    {
        Task<OperationResult<Guid>> CreateAsync(CreateUserInput input);
        Task<User> FindUserAsync(FindUserInput input);
        Task<IEnumerable<IdName>> GetUsersAsync();
    }
}

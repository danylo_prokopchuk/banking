﻿using Banking.Business.UnitTest.AsyncLinq;
using System.Collections.Generic;
using System.Linq;

namespace Banking.Business.UnitTest.Extentions
{
    public static class ListExtentions
    {
        public static IQueryable<T> AsAsyncQuaryable<T>(this List<T> list)
        {
            return new AsyncEnumerable<T>(list);
        }
    }
}

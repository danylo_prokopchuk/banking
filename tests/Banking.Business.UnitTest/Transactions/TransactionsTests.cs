﻿using Banking.Business.Transactions;
using Banking.Business.Transactions.Models;
using Banking.Business.UnitTest.Extentions;
using Banking.Business.UnitTest.Factory;
using Banking.Common.Models;
using Banking.DataAccess.Interfaces.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.Business.UnitTest.Transactions
{
    [TestFixture]
    public class TransactionsTests
    {
        private ITransactionsService _transactionsService;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<ITransactionsRepository> _transactionsRepository;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _transactionsRepository = new Mock<ITransactionsRepository>();
            _unitOfWork.SetupGet(u => u.TransactionsRepository).Returns(_transactionsRepository.Object);
            _transactionsService = new TransactionsService(_unitOfWork.Object);
        }

        [Test]
        public async Task TransactionsListAsync_WrongUser_EmptyList()
        {
            //Arrange
            var transactions = EntitiesFactory.GetTransactionsList();
            _transactionsRepository.Setup(x => x.GetAll()).Returns(transactions.AsAsyncQuaryable());
            TransactionsListInput input = new TransactionsListInput
            {
                UserId = Guid.Parse("15A748CD-A046-4514-A50A-3113972FE740"),
            };

            //Act
            var result = await _transactionsService.TransactionsListAsync(input);

            //Assert
            Assert.AreEqual(0, result.Items.Count());
        }

        [Test]
        public async Task TransactionsListAsync_CorrectUser_DataList()
        {
            //Arrange
            var transactions = EntitiesFactory.GetTransactionsList();
            _transactionsRepository.Setup(x => x.GetAll()).Returns(transactions.AsAsyncQuaryable());
            TransactionsListInput input = new TransactionsListInput
            {
                UserId = EntitiesFactory.GetUsersList()[0].Id
            };

            //Act
            var result = await _transactionsService.TransactionsListAsync(input);

            //Assert
            Assert.AreEqual(3, result.Items.Count());
            Assert.AreEqual(1, result.PageNumber);
            Assert.AreEqual(3, result.TotalRecords);
            Assert.AreEqual(PaginationResult<TransactionListItem>.DEFAULT_PAGE_SIZE, result.PageSize);
        }

        [Test]
        public async Task TransactionsListAsync_PaginationSeted_DataList()
        {
            //Arrange
            var transactions = EntitiesFactory.GetTransactionsList();
            _transactionsRepository.Setup(x => x.GetAll()).Returns(transactions.AsAsyncQuaryable());
            TransactionsListInput input = new TransactionsListInput
            {
                UserId = EntitiesFactory.GetUsersList()[0].Id,
                Take = 1,
                Skip = 2
            };

            //Act
            var result = await _transactionsService.TransactionsListAsync(input);

            //Assert
            Assert.AreEqual(1, result.Items.Count());
            Assert.AreEqual(3, result.PageNumber);
            Assert.AreEqual(3, result.TotalRecords);
            Assert.AreEqual(input.Take, result.PageSize);
        }

    }
}

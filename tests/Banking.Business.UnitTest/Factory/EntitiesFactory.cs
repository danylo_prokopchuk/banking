﻿using Banking.Common.Authorization;
using Banking.DataAccess.Entities;
using Banking.DataAccess.Entities.Enums;
using System;
using System.Collections.Generic;

namespace Banking.Business.UnitTest.Factory
{
    internal static class EntitiesFactory
    {

        private static readonly PasswordHasher _passwordHasher = new PasswordHasher();

        public static List<User> GetUsersList()
        {
            return new List<User>
            {
                new User{
                    Id = Guid.Parse("012165e9-8841-4179-8e65-c81cd6311244"),
                    Username = "username1",
                    Balance = 23,                    
                    PasswordHash = _passwordHasher.HashPassword("1111")
                    },
                 new User{
                    Id = Guid.Parse("A9F7EDB3-DE41-46D3-BC8C-E27D5D64D451"),
                    Username = "username2",
                    Balance = (decimal)32.45,
                    PasswordHash = _passwordHasher.HashPassword("2222")
                    },
                  new User{
                    Id = Guid.Parse("51839444-6BF6-4C57-9BA7-1A2BA7D34D5F"),
                    Username = "username3",
                    Balance = 43,
                    PasswordHash = _passwordHasher.HashPassword("3333")
                    },
                    new User{
                    Id = Guid.Parse("2368E890-1D68-4679-B269-26DAD11B2314"),
                    Username = "username4",
                    Balance = 12,
                    PasswordHash = _passwordHasher.HashPassword("4444")
                    },
            };
        }

        public static List<Transaction> GetTransactionsList()
        {
            var users = GetUsersList();
            var list = new List<Transaction>
            {
                new Transaction
                {
                    Amount = 21,
                    CreatedAt = DateTime.Now,
                    OperationType = TransactionOperationEnum.Deposit,
                    Owner = users[0],
                    OwnerId = users[0].Id,
                    Reciever =new User()
                },
                new Transaction
                {
                    Amount = 123,
                    CreatedAt = DateTime.Now,
                    OperationType = TransactionOperationEnum.Transfer,
                    OwnerId = users[1].Id,
                    Owner = users[1],
                    ReceiverId = users[0].Id,
                    Reciever = users[0]
                },
                new Transaction
                {
                    Amount = 353,
                    CreatedAt = DateTime.Now,
                    OperationType = TransactionOperationEnum.Withdraw,
                    OwnerId = users[2].Id,
                    Owner = users[2],
                    Reciever =new User()
                },
                 new Transaction
                {
                    Amount = (decimal)12.26,
                    CreatedAt = DateTime.Now,
                    OperationType = TransactionOperationEnum.Withdraw,
                    OwnerId = users[3].Id,
                    Owner = users[3],
                    Reciever = users[0],
                    ReceiverId = users[0].Id
                },

            };

            return list;
        }

    }
}

﻿using Banking.DataAccess;
using Banking.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Banking.Business.UnitTest.Factory
{
    public static class ContextFactory
    {
        public static ApplicationContext GetApplicationContext(string dbName)
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            var context = new ApplicationContext(options);
            //context.Users.AddRange(EntitiesFactory.GetUsersList());
            //context.Transactions.AddRange(EntitiesFactory.GetTransactionsList());

            return context;
        }
    }
}

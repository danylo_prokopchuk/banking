﻿using Banking.Business.Operations;
using Banking.Business.Operations.Models;
using Banking.Business.UnitTest.Extentions;
using Banking.Business.UnitTest.Factory;
using Banking.Common.Authorization;
using Banking.DataAccess.Entities;
using Banking.DataAccess.Interfaces.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Banking.Business.UnitTest.Operations
{
    [TestFixture]
    public class OperationsTests
    {
        private IOperationsService _operationsService;
        private Mock<ITransactionsRepository> _transactionsRepository;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IUsersRepository> _usersRepository;
        private PasswordHasher _passwordHasher;
        private int _concurrencyCalls;

        [SetUp]
        public void SetUp()
        {
            _concurrencyCalls = 0;
            _passwordHasher = new PasswordHasher();
            _usersRepository = new Mock<IUsersRepository>();
            _transactionsRepository = new Mock<ITransactionsRepository>();
            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.SetupGet(u => u.UsersRepository).Returns(_usersRepository.Object);
            _unitOfWork.SetupGet(u => u.TransactionsRepository).Returns(_transactionsRepository.Object);
            _operationsService = new OperationsService(_unitOfWork.Object);
        }

        #region DepositMoney

        [Test]
        public async Task DepositMoneyAsync_NonExistingUser_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var depositInput = new DepositInput
            {
                Amount = 32,
                UserId = Guid.Parse("7671CA66-81B2-4CF7-8BA2-5D015957C409")
            };


            //Act
            var result = await _operationsService.DepositMoneyAsync(depositInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(1, result.ErrorCode);
        }

        [Test]
        public async Task DepositMoneyAsync_NegativeAmount_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var depositInput = new DepositInput
            {
                Amount = -56,
                UserId = users[1].Id
            };


            //Act
            var result = await _operationsService.DepositMoneyAsync(depositInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(3, result.ErrorCode);
        }

        [Test]
        public async Task DepositMoneyAsync_ValidInput_SuccessResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var depositInput = new DepositInput
            {
                Amount = (decimal)23.23,
                UserId = users[1].Id
            };


            //Act
            var result = await _operationsService.DepositMoneyAsync(depositInput);

            //Assert
            Assert.AreEqual(true, result.Success);
            _transactionsRepository.Verify(x => x.Insert(It.IsAny<Transaction>()), Times.Once);
            _unitOfWork.Verify(x => x.SubmitChangesAsync(), Times.Once);
        }

        [Test]
        public async Task DepositMoneyAsync_ConcurrencyException_SuccessResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            ConfigureConcurrencyException("DepositMoneyAsync_ConcurrencyException_SuccessResult");

            var depositInput = new DepositInput
            {
                Amount = (decimal)23.23,
                UserId = users[1].Id
            };

            //Act
            var result = await _operationsService.DepositMoneyAsync(depositInput);

            //Assert
            Assert.AreEqual(true, result.Success);
            _transactionsRepository.Verify(x => x.Insert(It.IsAny<Transaction>()), Times.Once);
            _unitOfWork.Verify(x => x.SubmitChangesAsync(), Times.Exactly(2));
        }


        #endregion

        #region WithdrawMoney

        [Test]
        public async Task WithdrawMoneyAsync_NonExistingUser_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var withdrawInput = new WithdrawInput
            {
                Amount = 32,
                UserId = Guid.Parse("7671CA66-81B2-4CF7-8BA2-5D015957C409")
            };


            //Act
            var result = await _operationsService.WithdrawMoneyAsync(withdrawInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(1, result.ErrorCode);
        }

        [Test]
        public async Task WithdrawMoneyAsync_NegativeAmount_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var withdrawInput = new WithdrawInput
            {
                Amount = -56,
                UserId = users[1].Id
            };


            //Act
            var result = await _operationsService.WithdrawMoneyAsync(withdrawInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(3, result.ErrorCode);
        }


        [Test]
        public async Task WithdrawMoneyAsync_ValidInput_SuccessResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var withdrawInput = new WithdrawInput
            {
                Amount = (decimal)23.23,
                UserId = users[1].Id
            };


            //Act
            var result = await _operationsService.WithdrawMoneyAsync(withdrawInput);

            //Assert
            Assert.AreEqual(true, result.Success);
            _transactionsRepository.Verify(x => x.Insert(It.IsAny<Transaction>()), Times.Once);
            _unitOfWork.Verify(x => x.SubmitChangesAsync(), Times.Once);
        }


        [Test]
        public async Task WithdrawMoneyAsync_ConcurrencyException_SuccessResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            ConfigureConcurrencyException("WithdrawMoneyAsync_ConcurrencyException_SuccessResult");

            var withrawInput = new WithdrawInput
            {
                Amount = (decimal)2.24,
                UserId = users[1].Id
            };


            //Act
            var result = await _operationsService.WithdrawMoneyAsync(withrawInput);

            //Assert
            Assert.AreEqual(true, result.Success);
            _transactionsRepository.Verify(x => x.Insert(It.IsAny<Transaction>()), Times.Once);
            _unitOfWork.Verify(x => x.SubmitChangesAsync(), Times.Exactly(2));
        }

        #endregion

        #region Transfer

        [Test]
        public async Task TransferMoneyAsync_NonExistingReciever_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var transferInputInput = new TransferInput
            {
                Amount = 32,
                RecieverId = Guid.Parse("7671CA66-81B2-4CF7-8BA2-5D015957C409"),
                SenderId = users[0].Id
            };


            //Act
            var result = await _operationsService.TransferMoneyAsync(transferInputInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(1, result.ErrorCode);
        }

        [Test]
        public async Task TransferMoneyAsync_NonExistingSender_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var transferInputInput = new TransferInput
            {
                Amount = 32,
                SenderId = Guid.Parse("7671CA66-81B2-4CF7-8BA2-5D015957C409"),
                RecieverId = users[0].Id
            };


            //Act
            var result = await _operationsService.TransferMoneyAsync(transferInputInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(1, result.ErrorCode);
        }

        [Test]
        public async Task TransferMoneyAsync_NegativeAmount_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var transferInputInput = new TransferInput
            {
                Amount = -32,
                RecieverId = users[1].Id,
                SenderId = users[0].Id
            };


            //Act
            var result = await _operationsService.TransferMoneyAsync(transferInputInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(3, result.ErrorCode);
        }

        [Test]
        public async Task TransferMoneyAsync_SenderInsufficientFunds_ErrorResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(users.AsAsyncQuaryable());
            var transferInputInput = new TransferInput
            {
                Amount = 32,
                RecieverId = users[1].Id,
                SenderId = users[0].Id
            };


            //Act
            var result = await _operationsService.TransferMoneyAsync(transferInputInput);

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.AreEqual(2, result.ErrorCode);
        }

        [Test]
        public async Task TransferMoneyAsync_ConcurrencyException_SuccessResult()
        {
            // Arrange    
            var users = EntitiesFactory.GetUsersList();
            ConfigureConcurrencyException("TransferMoneyAsync_ConcurrencyException_SuccessResult");

            var transferInput = new TransferInput
            {
                Amount = (decimal)2.23,
                RecieverId = users[1].Id,
                SenderId = users[0].Id
            };


            //Act
            var result = await _operationsService.TransferMoneyAsync(transferInput);

            //Assert
            Assert.AreEqual(true, result.Success);
            _transactionsRepository.Verify(x => x.Insert(It.IsAny<Transaction>()), Times.Once);
            _unitOfWork.Verify(x => x.SubmitChangesAsync(), Times.Exactly(3));
        }

        #endregion

        private void ConfigureConcurrencyException(string dbName)
        {
            var users = EntitiesFactory.GetUsersList();
            var context = ContextFactory.GetApplicationContext(dbName);
            context.Users.AddRange(users);
            context.SaveChanges();


            _usersRepository.Setup(x => x.GetAll()).Returns(context.Users);
            _unitOfWork.Setup(x => x.SubmitChangesAsync()).Returns(() =>
            {
                if (_concurrencyCalls == 0)
                {
                    var otherContext = ContextFactory.GetApplicationContext(dbName);
                    var usr = otherContext.Users.Find(users[1].Id);
                    usr.Balance = 11;
                    _concurrencyCalls++;
                    otherContext.SaveChanges();
                    return context.SaveChangesAsync();
                }
                return context.SaveChangesAsync();
            });
        }
    }
}

﻿using Banking.Business.Common.Models;
using Banking.Business.UnitTest.Extentions;
using Banking.Business.UnitTest.Factory;
using Banking.Business.Users;
using Banking.Business.Users.Model;
using Banking.Common.Authorization;
using Banking.DataAccess.Entities;
using Banking.DataAccess.Interfaces.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Banking.Business.UnitTest.Users
{
    [TestFixture]
    public class UserServiceTests
    {

        private IUsersService _usersService;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IUsersRepository> _usersRepository;
        private PasswordHasher _passwordHasher;

        [SetUp]
        public void SetUp()
        {
            _passwordHasher = new PasswordHasher();
            _usersRepository = new Mock<IUsersRepository>();
            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.SetupGet(u => u.UsersRepository).Returns(_usersRepository.Object);
            _usersService = new UsersService(_passwordHasher, _unitOfWork.Object);
        }


        [Test]
        [TestCase(new object[] { null, null })]
        [TestCase(new object[] { "", null })]
        [TestCase(new object[] { null, "" })]
        [TestCase(new object[] { "", "" })]
        public async Task CreateAsync_InvalidInput_ReturnNonSuccess(string username, string password)
        {
            //Act
            var result = await _usersService.CreateAsync(new CreateUserInput());

            //Assert
            Assert.AreEqual(false, result.Success);
            Assert.IsTrue(result.ErrorCode == 1 || result.ErrorCode == 2);
        }

        #region CreateAsync

        [Test]
        public async Task CreateAsync_ExistingUser_ReturnNonSuccess()
        {
            // Arrange            
            var usersList = EntitiesFactory.GetUsersList();
            var userToCreate = new CreateUserInput { Username = usersList[0].Username, Password = "password" };
            _usersRepository.Setup(x => x.GetAll()).Returns(usersList.AsQueryable());

            //Act
            var result = await _usersService.CreateAsync(userToCreate);

            //Assert
            Assert.IsFalse(result.Success);
            Assert.AreEqual(3, result.ErrorCode);
        }

        [Test]
        public async Task CreateAsync_NonExistingUser_ReturnSuccess()
        {
            // Arrange                       
            var usersList = EntitiesFactory.GetUsersList();
            var userToCreate = new CreateUserInput { Username = "username", Password = "password" };
            _usersRepository.Setup(x => x.GetAll()).Returns(usersList.AsQueryable());

            //Act
            var result = await _usersService.CreateAsync(userToCreate);

            //Assert
            Assert.IsTrue(result.Success);
            Assert.AreNotEqual(result.Object, Guid.Empty);
            _usersRepository.Verify(x => x.Insert(It.IsAny<User>()), Times.Once);
            _unitOfWork.Verify(x => x.SubmitChangesAsync(), Times.Once);
        }

        #endregion


        #region Find

        [Test]
        [TestCase(new object[] { null, null })]
        [TestCase(new object[] { "username", "" })]
        [TestCase(new object[] { null, "dsgsdgsdd" })]
        public async Task FindAsync_WrongUserInfo_ReturnNull(string username, string password)
        {
            // Arrange            
            var usersList = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(usersList.AsAsyncQuaryable());

            //Act
            var result = await _usersService.FindUserAsync(new FindUserInput
            {
                Username = username,
                Password = password
            });

            //Assert
            Assert.IsNull(result);
        }

        [Test]
        public async Task FindAsync_RightUserInfo_ReturnObject()
        {
            // Arrange            
            var usersList = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(usersList.AsAsyncQuaryable());

            //Act
            var result = await _usersService.FindUserAsync(new FindUserInput
            {
                Username = "username1",
                Password = "1111"
            });

            //Assert
            Assert.IsInstanceOf<User>(result);
        }

        #endregion

        #region GetDictionary


        [Test]
        public async Task GetDictionaryAsync_WhenCall_ReturnData()
        {
            // Arrange            
            var usersList = EntitiesFactory.GetUsersList();
            _usersRepository.Setup(x => x.GetAll()).Returns(usersList.AsAsyncQuaryable());
            var expectedList = usersList.Select(x => new IdName { Id = x.Id, Name = x.Username }).ToList();

            //Act
            var result = await _usersService.GetUsersAsync();

            //Assert
            int iterator = 0;
            foreach (var item in result)
            {
                Assert.AreEqual(expectedList[iterator].Id, item.Id);
                Assert.AreEqual(expectedList[iterator].Name, item.Name);
                iterator++;
            }


        }


        #endregion
    }
}
